import './App.scss';
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom"
import Drop from './pages/Drop'
import CalendarJobDating from './pages/Calendar/JobDating'
import CalendarSoutenances from './pages/Calendar/Soutenances'
import DataExamples from './pages/DataExamples';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/calendar/job-dating" element={<CalendarJobDating />} />
        <Route path="/calendar/soutenances" element={<CalendarSoutenances />} />
        <Route path="/data-examples" element={<DataExamples />} />
        <Route path="/" element={<Drop />} />
        <Route path='*' element={<Navigate to='/' />} />
      </Routes>
    </BrowserRouter>
  );
}
export default App;
