import Organizer from '../../components/Organizer';
import AppHeader from '../../components/AppHeader'
import AppFooter from '../../components/AppFooter';

export default function CalendarJobDating(){
    let companies = JSON.parse(localStorage.getItem("dataJobDating"));

    return(
        <div className="App">
            <AppHeader></AppHeader>
            <Organizer data={companies}></Organizer>
            <AppFooter></AppFooter>
        </div>
    )
}