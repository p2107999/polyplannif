import Organizer from '../../components/Organizer';
import AppHeader from '../../components/AppHeader'
import AppFooter from '../../components/AppFooter';

export default function CalendarSoutenance(){
    let soutenances = JSON.parse(localStorage.getItem("dataSoutenances"));

    return(
        <div className="App">
            <AppHeader></AppHeader>
            <Organizer data={soutenances}></Organizer>
            <AppFooter></AppFooter>
        </div>
    )
}