import AppHeader from '../components/AppHeader'
import AppFooter from '../components/AppFooter';
import Table from 'react-bootstrap/Table';
import { ArrowDownCircle } from "react-bootstrap-icons";
import '../styles/DataExamples.scss'

function DataExamples() {
  return (
    <div className="App">
      <AppHeader></AppHeader>
      <div className="data-examples">
        <p>
          Vous trouverez ici des fichiers Excel servant d'exemple afin que vous puissiez concevoir les votres :
        </p>
        <Table bordered>
          <thead>
            <tr>
              <td>Type de donnée</td>
              <td>Téléchargement</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Job dating</td>
              <td><a href="/job-dating-mock-data.xlsx" download><ArrowDownCircle /></a></td>
            </tr>
            <tr>
              <td>Soutenances</td>
              <td><a href="/soutenances-mock-data.xlsx"><ArrowDownCircle /></a></td>
            </tr>
          </tbody>
        </Table>
      </div>
      <AppFooter></AppFooter>
    </div>
  );
}
export default DataExamples;
