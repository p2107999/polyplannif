import AppHeader from '../components/AppHeader'
import AppFooter from '../components/AppFooter';
import DropBox from '../components/DropBox';

function Drop() {
  return (
    <div className="App">
      <AppHeader></AppHeader>
      <DropBox></DropBox>
      <AppFooter></AppFooter>
    </div>
  );
}
export default Drop;
