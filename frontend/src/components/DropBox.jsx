import "../styles/DropBox.scss"
import { useRef, useEffect, useState } from "react";
import { useNavigate  } from "react-router-dom";
import Loader from "./Loader";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const JOB_DATING_ROUTE = '/calendar/job-dating';
const SOUTENANCES_ROUTE = '/calendar/soutenances';

export default function DropBox() {
    const uploadFile = useRef(null)
    const [uploadFileTitle, setuploadFileTitle] = useState(null)
    const [file, setFile] = useState(null)
    const [eventType, setEventType] = useState(false)
    let navigate = useNavigate ()
    let route = useRef(JOB_DATING_ROUTE)
    let [loading, setLoading] = useState(true);
    

    const sendPostRequest = (file) => {
        let formData = new FormData();
        formData.append('file', file);
        let url = null;
        let key = null;

        if (route.current === JOB_DATING_ROUTE) {
            url = process.env.REACT_APP_API_URL + 'jobs/jobdating/';
            key = 'JobDating';
        }

        if (route.current === SOUTENANCES_ROUTE) {
            url = process.env.REACT_APP_API_URL + 'viva/viva/';
            key = 'Soutenances';
        }
        
        fetch(url, {
            method: 'POST',
            body: formData
        })
        .then(response => {
            if (!response.ok) { 
                throw new Error('Erreur réseau');
            }
            return response.json();
        })
        .then(data => {
            localStorage.setItem("data" + key, JSON.stringify(data))
            localStorage.setItem("date" + key, JSON.stringify(new Date().toLocaleTimeString('fr-FR',{day:'2-digit', month:'2-digit', year:'numeric',hour:'2-digit',minute:'2-digit'})))
            navigate(route.current)
        })
        .catch(error => {
            setLoading(true);
            toast.error('Erreur serveur', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                pauseOnHover: true,
                theme: "colored",
            });
            console.error('Erreur lors de la requête :', error);
        });
    }

    const validateFile = () => {
        if(file != null){
            sendPostRequest(file);
        }
    }

    const handleFiles = ()=> {
        if (uploadFile.current) {
            const { files } = uploadFile.current
            setFile(files[0])
            setuploadFileTitle(files[0].name)
        }
    }
    useEffect(() => {
        if (uploadFile.current) {
            uploadFile.current.addEventListener('change', handleFiles, false)
        }
        return () => {
            uploadFile.current && uploadFile.current.removeEventListener('change', handleFiles, false)
        }
    }, [uploadFile])
    
    const handleEventTypeChange = event => {
        let date = null
        
        if (eventType)
        {
            route.current = JOB_DATING_ROUTE
            date = JSON.parse(localStorage.getItem("dateJobDating"))
        } else {
            route.current = SOUTENANCES_ROUTE
            date = JSON.parse(localStorage.getItem("dateSoutenances"))
        }

        document.querySelector('.last-data').innerHTML = dateHtml(date, route.current)
        setEventType(event.target.checked)
    }

    function dateHtml(date, route) {
        let html = null
        date ? html = `Des données datant du <span class=\"date\">${date}</span> sont déjà présentes en mémoire. <a href=\"${route}\">Voulez-vous y accéder ?</a>` : html = null

        return html
    }

    return (
        <div>
            <ToastContainer />
            <Loader loading={loading} />
            <div className="content">
                <div className="drop-box">
                    <p className="last-data" dangerouslySetInnerHTML={{ __html: dateHtml(JSON.parse(localStorage.getItem('dateJobDating')), JOB_DATING_ROUTE)}}></p>
                    <div className="drop-box-zone">
                        <input 
                            id="file" 
                            type="file"
                            ref={ uploadFile } 
                            accept=".xlsx"
                            />
                            <p>
                            {
                                uploadFileTitle === null ?   
                                "Drop an Excel or a CSV file" :
                                uploadFileTitle  
                            }
                            </p>
                    </div>
                </div>
                <div className="validation-form">
                    <span className="type-title">Job Dating</span>
                    <label class="switch">
                        <input type="checkbox"  checked={eventType} onChange={handleEventTypeChange} />
                        <span class="slider round"></span>
                    </label>
                    <span className="type-title">Soutenance</span>
                    <button className="validate-button" onClick={() => {validateFile(); setLoading(!loading)}}>Valider</button>
                </div>
            </div>
        </div>
    )
}