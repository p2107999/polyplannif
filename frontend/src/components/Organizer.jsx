import '../styles/Organizer.scss'
import { useRef, useEffect, useState } from "react";
import Sliders from './Slider/Sliders';
import Calendar from './Calendar/Calendar';
import DownloadButton from './DownloadButton';

function Organizer({data}) {
  const [jsonData, setJsonData] = useState(null);

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = (e) => {  
      const contents = e.target.result;
      setJsonData(contents);
    };

    reader.readAsText(file);
  };

    return (
        <>
            <div className="options">
              <input type="file" id="json-data" name="json-data" onChange={handleFileChange} />
              <button type="button"><label for="json-data">Utiliser un JSON</label></button>
              <DownloadButton data={jsonData ? JSON.parse(jsonData) : data} />
            </div>
            <Sliders elements={jsonData ? JSON.parse(jsonData) : data} />
            <Calendar elements={jsonData ? JSON.parse(jsonData) : data} />
        </>
    )
}

export default Organizer;