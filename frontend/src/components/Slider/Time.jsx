import React, { useEffect, useState } from "react";
import { CaretLeftFill, CaretRightFill } from "react-bootstrap-icons";

function Time({ elements }) {
  const [dates, setDates] = useState([]);
  const [selectedIndex, setSelectedIndex] = useState(0);

  useEffect(() => {
    // Extraction des dates uniques des éléments
    const uniqueDates = elements.reduce((acc, element) => {
      element.timeSlots.forEach((timeSlot) => {
        if (!acc.includes(timeSlot.date)) {
          acc.push(timeSlot.date);
        }
      });
      return acc;
    }, []);
    setDates(uniqueDates);
  }, [elements]);

  useEffect(() => {
    // Mettre à jour la vue lorsque l'index sélectionné change
    updateView();
  }, [selectedIndex, dates]);

  const updateView = () => {
    // Mettre à jour la vue en utilisant l'état local selectedIndex
    const currentDate = dates[selectedIndex];
    const elementsToShow = document.querySelectorAll(`.event[data-date="${currentDate}"]`);
    const allElements = document.querySelectorAll('.event');

    allElements.forEach((appointment) => {
      appointment.classList.add("hidden");
    });

    elementsToShow.forEach((appointment) => {
      appointment.classList.remove("hidden");
    });

    document.querySelector('.sliders .time .current').innerHTML = "Date : " + currentDate;
  };

  const handlePreviousClick = () => {
    setSelectedIndex((prevIndex) => (prevIndex === 0 ? dates.length - 1 : prevIndex - 1));
  };

  const handleNextClick = () => {
    setSelectedIndex((prevIndex) => (prevIndex === dates.length - 1 ? 0 : prevIndex + 1));
  };

  return (
    <div className="time">
      <button className="previous" onClick={handlePreviousClick}><CaretLeftFill /></button>
      <span className="current">Date : {dates.length > 0 && dates[selectedIndex]}</span>
      <button className="next" onClick={handleNextClick}><CaretRightFill /></button>
    </div>
  );
}

export default Time;
