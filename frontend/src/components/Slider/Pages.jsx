import React, { useEffect, useState } from "react";
import { CaretLeftFill, CaretRightFill } from "react-bootstrap-icons";

function Pages({ elements }) {
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [totalPages, setTotalPages] = useState(0);

  useEffect(() => {
    // Calculer le nombre total de pages
    setTotalPages(Math.ceil(elements.length / 6));
  }, [elements]);

  useEffect(() => {
    // Mettre à jour la vue lorsque l'index sélectionné change
    updateView();
  }, [selectedIndex, totalPages]);

  const updateView = () => {
    // Mettre à jour la vue en utilisant l'état local selectedIndex
    const currentPage = selectedIndex + 1;
    document.querySelector('.sliders .elements .current').innerHTML = "Page : " + currentPage;

    const elementsToShow = document.querySelectorAll(`.element[data-page="${selectedIndex}"]`);
    const allElements = document.querySelectorAll('.element');

    allElements.forEach((element) => {
      element.classList.add("hidden");
    });

    elementsToShow.forEach((element) => {
      element.classList.remove("hidden");
    });
  };

  const handlePreviousClick = () => {
    setSelectedIndex((prevIndex) => (prevIndex === 0 ? totalPages - 1 : prevIndex - 1));
  };

  const handleNextClick = () => {
    setSelectedIndex((prevIndex) => (prevIndex === totalPages - 1 ? 0 : prevIndex + 1));
  };

  return (
    <div className="elements">
      <button className="previous" onClick={handlePreviousClick}><CaretLeftFill /></button>
      <span className="current">Page : {selectedIndex + 1}</span>
      <button className="next" onClick={handleNextClick}><CaretRightFill /></button>
    </div>
  );
}

export default Pages;