import React from "react";
import Time from "./Time";
import Pages from "./Pages";

function Sliders({elements}) {
    return (
        <div className="sliders">
            <Time elements={elements} />
            <Pages elements={elements} />
        </div>
    )
}

export default Sliders;