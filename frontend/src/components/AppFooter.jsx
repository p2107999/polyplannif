import '../styles/AppFooter.scss'

export default function AppFooter() {
    return (
        <footer>
            <img className="logo" src="/algovision.png" alt="Logo Algovision" />
            <span> - Promo n°12 - 2023/2024</span>
        </footer>
    )
}