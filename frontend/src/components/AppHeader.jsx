import '../styles/AppHeader.scss'
import { useNavigate  } from "react-router-dom"

export default function AppHeader() {
    let navigate = useNavigate ()

    return (
        <header>
            <img className="logo" src="/logo.png" alt="Logo Polytech Lyon" onClick={() => {navigate("/")}} />
            <ul>
                <li><a href="/data-examples">Exemples de données</a></li>
            </ul>
        </header>
    )
}