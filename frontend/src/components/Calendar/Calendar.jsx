import Timeline from "./Timeline";
import Events from "./Events";

function Calendar({elements}) {
    return (
        <div className="calendar">
            <Timeline />
            <div className="elements">
                { elements.map((element, index) => (
                    <div className="element" data-page={Math.floor(index / 6)}>
                        <h2 className="name">{element.room !== undefined ? "Salle " + element.room : element.name}</h2>
                        <Events timeSlots={element.timeSlots} />
                    </div>
                )) }
            </div>
        </div>
    )
}

export default Calendar;