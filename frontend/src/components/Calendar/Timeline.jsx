function Timeline() {
    let timeMarkers = [];

    function formatNumber(number) {
        if (number < 10) {
          return "0" + number;
        } else {
          return number.toString();
        }
      }

    for(let i = 8; i <= 18; i++) {
        for(let j = 0; j < 60; j+=1) {
            if(j === 0 || j === 30) {
                timeMarkers.push(formatNumber(i) + "h" + formatNumber(j));
            } else {
                timeMarkers.push("");
            }

        }
    }
    return (
        <div className="timeline">
            { timeMarkers.map((timeMarker) => (
                    <div className="time-marker">{timeMarker}</div>
            )) }
        </div>
    )
}

export default Timeline;