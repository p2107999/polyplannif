import Appointments from "./Appointments";

function Events({timeSlots}) {
    return(
        <div className="events">
            {timeSlots.map((timeSlot) => (
                <Appointments date={timeSlot.date} appointments={timeSlot.appointments} />
            ))}
        </div>
    )
}

export default Events;