import Appointment from "./Appointment";

function Appointments({date, appointments}) {
    return(
        <>
            { appointments.map((appointment) => (
                <Appointment date={date} appointment={appointment}/>
            )) }
        </>
    )
}

export default Appointments;