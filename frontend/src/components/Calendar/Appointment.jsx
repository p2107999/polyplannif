function Appointment({date, appointment}) {
    return(
        <div className={"event start-" + parseInt(appointment.startTime.split(':')[0]) + "-" + Math.round(parseInt(parseInt(appointment.startTime.split(':')[1])) / 10) * 10 + " end-" + parseInt(appointment.endTime.split(':')[0]) + "-" + Math.round(parseInt(appointment.endTime.split(':')[1]) / 10) * 10 + " hidden"} data-date={date}>
            <p className="student">{appointment.student}</p>
            <p className="teachers">{appointment.firstTeacher} - {appointment.secondTeacher}</p>
            <p className="time">{appointment.startTime.split(':')[0]}:{appointment.startTime.split(':')[1]} - {appointment.endTime.split(':')[0]}:{appointment.endTime.split(':')[1]}</p>
        </div>
    )
}

export default Appointment;