import React from 'react';

function DownloadButton({data}) {
  const handleDownload = () => {
    const jsonData = JSON.stringify(data);
    const jsonDataBlob = new Blob([jsonData], { type: 'application/json' });
    const url = URL.createObjectURL(jsonDataBlob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'data.json';
    document.body.appendChild(a);
    a.click();

    document.body.removeChild(a);
    URL.revokeObjectURL(url);
  };

  return <button onClick={handleDownload}>Télécharger le JSON</button>;
}

export default DownloadButton;
