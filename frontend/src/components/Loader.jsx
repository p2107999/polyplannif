import ClipLoader from "react-spinners/ClipLoader";

export default function Loader({loading}) {
    return (
        <div className={loading ? null : "loader"}>
            <ClipLoader
                color={"##d1ffe6"}
                loading={!loading}
                size={150}
            />
        </div>
    )
}