import json
from typing import List

from fastapi import APIRouter, UploadFile, File, HTTPException

from service import excelConverter
from service.graphConverter import wishes_to_graph
from service.timetableService import calculerEmploiDuTemps
from model.pydantic.jobdating.CompanySchema import CompanySchema

router = APIRouter(
    prefix="/jobs",
    tags=["jobdating"],
    responses={404: {"description": "Not found"}},
)


@router.post(
    "/jobdating/",
    description="Endpoint to calculate a job dating planning",
    response_model=List[CompanySchema],
)
async def upload_jobdating(file: UploadFile = File(...)):
    try:
        file_contents = await file.read()
        timetable = calculerEmploiDuTemps(file_contents)
        return [elem.model_dump(by_alias=True) for elem in timetable]
    except Exception as e:
        print(e)
        raise e
        raise HTTPException(
            status_code=500, detail="Something went wrong, please contact the support"
        )
