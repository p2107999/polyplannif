from fastapi import APIRouter, Depends, HTTPException

router = APIRouter(
    prefix="/users",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)


@router.get("/")
async def test_users():
    return [{"name": "TestUSER"}, {"name": "TestUSER2"}]


@router.get("/{username}")
async def read_username(username: str):
    return {"name": "Hello "+username}
