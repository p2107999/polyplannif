from typing import List
from copy import deepcopy

from config import MICROSERVICE_URL
from fastapi import APIRouter, UploadFile, File, HTTPException
from service.excelConverter import convert_to_teachers_rooms
from model.pydantic.vivas.RoomSchema import RoomSchema
from service.graphService import join_prof_1, join_prof_2
from requests import post

"""
Routeur d'une organisation de présentations
"""

router = APIRouter(
    prefix="/viva",
    tags=["viva"],
    responses={404: {"description": "Not found"}},
)


# @router.post("/init/")
# async def receive_organisation_file(file: UploadFile = File(...)):
#     file_contents = await file.read()
#     return convert_to_teachers_rooms(file_contents)


def func(**kwargs):
    pass


@router.post(
    "/viva/",
    description="Endpoint to calculate viva planning",
    response_model=List[RoomSchema],
)
async def upload_viva(file: UploadFile = File(...)):
    try:
        file_contents = await file.read()
        it_teachers, non_it_teachers, rooms = convert_to_teachers_rooms(file_contents)

        teachers: list = deepcopy(it_teachers)
        teachers.extend(non_it_teachers)

        it_teachers_as_tuple = [
            [elem.name, len(elem.supervised_students)] for elem in it_teachers
        ]
        non_it_teachers_as_tuple = [
            [elem.name, len(elem.supervised_students)] for elem in non_it_teachers
        ]

        it_teachers_as_tuple_double = [
            [elem.name, len(elem.supervised_students) * 2] for elem in it_teachers
        ]
        non_it_teachers_as_tuple_double = [
            [elem.name, len(elem.supervised_students) * 2] for elem in non_it_teachers
        ]

        if sum([teacher[1] for teacher in it_teachers_as_tuple]) < sum(
            [teacher[1] for teacher in non_it_teachers_as_tuple]
        ):
            raise ValueError()

        choice_1_vivas = join_prof_1(
            it_teachers_as_tuple.copy(), non_it_teachers_as_tuple.copy()
        )
        choice_2_vivas = join_prof_2(
            it_teachers_as_tuple_double.copy(), non_it_teachers_as_tuple_double.copy()
        )

        choice_1_sum_vivas = sum([viva[2] for viva in choice_1_vivas])
        choice_2_sum_vivas = sum([viva[2] for viva in choice_2_vivas])

        vivas = []

        if choice_1_sum_vivas == choice_2_sum_vivas:
            choice_1_number_duos = len(choice_1_vivas)
            choice_2_number_duos = len(choice_2_vivas)
            if choice_1_number_duos < choice_2_number_duos:
                vivas = choice_1_vivas
            else:
                vivas = choice_2_vivas
        elif choice_1_sum_vivas < choice_2_sum_vivas:
            vivas = choice_1_vivas
        else:
            vivas = choice_2_vivas

        vivas_with_respective_student_count = [
            (*elem, elem[2] // 2 + (0 if elem[2] % 2 == 0 else 1), elem[2] // 2)
            for elem in vivas
        ]

        params = {
            "matchingTeachers": [
                {
                    "teacher1": elem[0],
                    "teacher2": elem[1],
                    "teacher1StudentCount": int(elem[3]),
                    "teacher2StudentCount": int(elem[4]),
                }
                for elem in vivas_with_respective_student_count
            ],
            "tutors": [
                {
                    "name": elem.name,
                    "students": [
                        {"name": elem.name} for elem in elem.supervised_students
                    ],
                }
                for elem in teachers
            ],
            "itTeachers": [
                {
                    "name": elem.name,
                    "students": [
                        {"name": elem.name} for elem in elem.supervised_students
                    ],
                }
                for elem in it_teachers
            ],
            "nonItTeachers": [
                {
                    "name": elem.name,
                    "students": [
                        {"name": elem.name} for elem in elem.supervised_students
                    ],
                }
                for elem in non_it_teachers
            ],
            "rooms": [{"name": elem.name} for elem in rooms],
        }

        response = post(f"{MICROSERVICE_URL}/generate/best-timetable", json=params,
                        headers={"Content-Type": "application/json"})

        timetable_data = response.json()

        rooms = [RoomSchema(**elem) for elem in timetable_data]

        return [room.model_dump(by_alias=True) for room in rooms]
    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=500, detail="Something went wrong, please contact the support"
        )
