from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from routers.jobdating import router as job_router
from routers.vivas import router as viva_rooter

from dotenv import load_dotenv
load_dotenv()

app = FastAPI(docs_url="/docs")
app.include_router(job_router)
app.include_router(viva_rooter)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
