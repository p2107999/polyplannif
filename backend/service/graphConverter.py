from datetime import timedelta, datetime
from typing import List

import networkx as nx

from model.company import Company
from model.student import Student
from model.voeux import Voeux
from model.pydantic.jobdating.CompanySchema import CompanySchema
from model.pydantic.jobdating.TimeSlotSchema import TimeSlotSchema
from model.pydantic.jobdating.AppointmentSchema import AppointmentSchema


def wishes_to_graph(wishes: list[Voeux]) -> nx.Graph:
    company_list: list[Company] = []
    student_list: list[Student] = []

    graph = nx.Graph()

    for wish in wishes:
        if wish.company not in company_list:
            company_list.append(wish.company)
        if wish.student not in student_list:
            student_list.append(wish.student)

    for company in company_list:
        graph.add_node(company.name, bipartite=0)
    for student in student_list:
        graph.add_node(student.name, bipartite=1)

    graph.add_edges_from([(wish.company.name, wish.student.name) for wish in wishes])

    return graph


def graph_to_front(
    graph: list[dict[Company, Student]], slots: List[dict]
) -> list[CompanySchema]:
    company_list: list[CompanySchema] = []
    max_slots = len(slots)
    number_time_slots = len(graph)
    today = datetime.now().date()

    for i in range(number_time_slots):
        delta = i // max_slots
        date = today + timedelta(days=delta)
        for student in graph[i]:
            appointment = AppointmentSchema(
                student=student,
                start_time=str(slots[i % max_slots].get("beginning")),
                end_time=str(slots[i % max_slots].get("end")),
            )
            if graph[i][student] not in company_list:
                company_list.append(
                    CompanySchema(
                        name=graph[i][student],
                        time_slots=[
                            TimeSlotSchema(date=str(date), appointments=[appointment])
                        ],
                    )
                )
            else:
                updated = False

                for time_slot in company_list[
                    company_list.index(graph[i][student])
                ].time_slots:
                    if time_slot.date == date:
                        time_slot.appointments.append(appointment)
                        updated = True

                if not updated:
                    company_list[
                        company_list.index(graph[i][student])
                    ].time_slots.append(
                        TimeSlotSchema(date=str(date), appointments=[appointment])
                    )

    return company_list
