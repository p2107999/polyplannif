import networkx as nx
from networkx import Graph
from networkx.algorithms import bipartite, greedy_color, equitable_color
from networkx.algorithms.bipartite import hopcroft_karp_matching

from model.company import Company
from model.student import Student

from service.graphVizualization import display_graph


def calculHopcroft(graph: Graph):
    """
    Calculate bipartite matching using the Hopcroft-Karp algorithm
    https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.bipartite.matching.hopcroft_karp_matching.html#networkx.algorithms.bipartite.matching.hopcroft_karp_matching
    """

    ordre_passage: list[dict[Company, Student]] = []
    bottom_nodes, top_nodes = bipartite.sets(graph)
    while graph.number_of_edges() > 0:
        try:
            matches = hopcroft_karp_matching(graph, top_nodes=top_nodes)

            split = dict(list(matches.items())[: len(matches) // 2])

            ordre_passage.append(split)
            # Remove the matches from the graph
            graph.remove_edges_from(split.items())

        except nx.AmbiguousSolution:
            break

    return ordre_passage


def equilibre_prof(info_prof: list, non_info_prof: list, couple_de_prof: list):
    diff = sum([elem[1] for elem in info_prof]) - sum(
        [elem[1] for elem in non_info_prof]
    )

    # si ma différence est plus grande que le professeur qui à le plus d'élève => il faut transférer plusieurs profs
    while info_prof[0][1] <= (diff / 2):
        non_info_prof.append(info_prof[0])
        non_info_prof.sort(key=lambda x: x[1], reverse=True)
        diff -= info_prof[0][1] * 2
        del info_prof[0]

    while diff > 0:
        prof_transfert = False

        # si la différence est égale à l'un des profs, je transfert un prof
        for elem in info_prof.copy():
            if elem[1] == diff / 2:
                non_info_prof.append(elem)
                non_info_prof.sort(key=lambda x: x[1], reverse=True)
                diff -= elem[1] * 2
                info_prof.remove(elem)
                prof_transfert = True

        # sinon je transmet une partie du premier
        if prof_transfert == False:
            # Si le deuxième élément du tableau est égal ou inférieur au nombre de créneau de diff/2
            if info_prof[1][1] <= diff / 2:
                couple_de_prof.append(
                    (info_prof[0][0], info_prof[1][0], info_prof[1][1])
                )

                info_prof[0][1] -= info_prof[1][1]
                diff -= info_prof[1][1] * 2
                del info_prof[1]

            else:
                couple_de_prof.append((info_prof[0][0], info_prof[1][0], int(diff / 2)))
                info_prof[1][1] -= diff / 2
                info_prof[0][1] -= diff / 2

                diff = 0

    return info_prof, non_info_prof, couple_de_prof


def join_prof_2(info_prof, non_info_prof):
    couple_de_prof = []

    info_prof.sort(key=lambda x: x[1], reverse=True)
    non_info_prof.sort(key=lambda x: x[1], reverse=True)

    info_prof, non_info_prof, couple_de_prof = equilibre_prof(
        info_prof, non_info_prof, couple_de_prof
    )
    # coupler les profs avec le même nombre d'heure
    indexnon_info_prof = 0
    indexinfo_prof = 0
    while indexinfo_prof < len(info_prof) and indexnon_info_prof < len(non_info_prof):
        if info_prof[indexinfo_prof][1] == non_info_prof[indexnon_info_prof][1]:
            couple_de_prof.append(
                (
                    info_prof[indexinfo_prof][0],
                    non_info_prof[indexnon_info_prof][0],
                    info_prof[indexinfo_prof][1],
                )
            )
            del info_prof[indexinfo_prof]
            del non_info_prof[indexnon_info_prof]

        elif info_prof[indexinfo_prof][1] > non_info_prof[indexnon_info_prof][1]:
            indexinfo_prof += 1

        else:
            indexnon_info_prof += 1

    # coupler les prof restant
    while len(non_info_prof) > 0:
        if info_prof[0][1] > non_info_prof[0][1]:
            couple_de_prof.append(
                (info_prof[0][0], non_info_prof[0][0], non_info_prof[0][1])
            )
            info_prof[0][1] -= non_info_prof[0][1]
            del non_info_prof[0]

        elif info_prof[0][1] == non_info_prof[0][1]:
            couple_de_prof.append(
                (info_prof[0][0], non_info_prof[0][0], non_info_prof[0][1])
            )
            del info_prof[0]
            del non_info_prof[0]

        else:
            couple_de_prof.append(
                (
                    info_prof[0][0],
                    non_info_prof[0][0],
                    info_prof[0][1],
                )
            )
            non_info_prof[0][1] -= info_prof[0][1]
            del info_prof[0]

    # coupler les profs info qui ont le même nombre d'heure
    indexinfo_prof = 0
    while indexinfo_prof < len(info_prof) - 1:
        if info_prof[indexinfo_prof][1] == info_prof[indexinfo_prof + 1][1]:
            couple_de_prof.append(
                (
                    info_prof[indexinfo_prof][0],
                    info_prof[indexinfo_prof + 1][0],
                    info_prof[indexinfo_prof][1],
                )
            )
            del info_prof[indexinfo_prof + 1]
            del info_prof[indexinfo_prof]
        else:
            indexinfo_prof += 1

    # coupler le reste des profs d'info
    while len(info_prof) > 0:
        if info_prof[0][1] > info_prof[1][1]:
            couple_de_prof.append((info_prof[0][0], info_prof[1][0], info_prof[1][1]))
            info_prof[0][1] -= info_prof[1][1]
            del info_prof[1]

        elif info_prof[0][1] == info_prof[1][1]:
            couple_de_prof.append((info_prof[0][0], info_prof[1][0], info_prof[1][1]))
            del info_prof[1]
            del info_prof[0]

        else:
            couple_de_prof.append((info_prof[0][0], info_prof[1][0], info_prof[0][1]))
            info_prof[1][1] -= info_prof[0][1]
            del info_prof[1]

    return couple_de_prof


def join_prof_1(infoProf, nonInfoProf):
    coupleDeProf = []

    nb_it_meetings = sum([elem[1] for elem in infoProf])
    nb_non_it_meetings = sum([elem[1] for elem in nonInfoProf])

    over_it_meetings = nb_it_meetings - nb_non_it_meetings
    nb_it_matching = over_it_meetings // 2
    is_odd_matching = over_it_meetings % 2 == 1

    max_nb = max([elem[1] for elem in infoProf] + [elem[1] for elem in nonInfoProf])

    nb_of_it_occurences = {occ: 0 for occ in range(0, max_nb + 1)}
    nb_of_it_occurences_list = {occ: [] for occ in range(0, max_nb + 1)}
    nb_of_non_it_occurences = nb_of_it_occurences.copy()
    nb_of_non_it_occurences_list = {occ: [] for occ in range(0, max_nb + 1)}
    difference_occurences = nb_of_it_occurences.copy()

    for elem in infoProf:
        nb_of_it_occurences[elem[1]] += 1
        nb_of_it_occurences_list[elem[1]].append(elem)
    for elem in nonInfoProf:
        nb_of_non_it_occurences[elem[1]] += 1
        nb_of_non_it_occurences_list[elem[1]].append(elem)

    for key in difference_occurences:
        difference_occurences[key] = nb_of_it_occurences.get(
            key, 0
        ) - nb_of_non_it_occurences.get(key, 0)

    # Essayer une liste des profs qui peuvent servir à lisser la différences en nombre de tutorats
    # Trié par orde decroissant de soutenances à effectuer
    # Prendre les profs en partant de la gauche
    # print(max({k:v for k,v in difference_occurences.items() if v}.keys()))
    # print(nb_of_it_occurences_list)
    # print(nb_of_it_occurences_list[max({k:v for k,v in difference_occurences.items() if v}.keys())])
    while nb_it_matching > 0:
        index = max(
            {
                k: v
                for k, v in difference_occurences.items()
                if nb_of_it_occurences_list[k]
            }.keys()
        )
        first_prof = nb_of_it_occurences_list[index].pop(0)
        index = max(
            {
                k: v
                for k, v in difference_occurences.items()
                if nb_of_it_occurences_list[k]
            }.keys()
        )
        second_prof = nb_of_it_occurences_list[index].pop(0)
        min_value = min(first_prof[1], second_prof[1], nb_it_matching)
        nb_it_matching -= min_value
        first_prof[1] -= min_value
        second_prof[1] -= min_value
        coupleDeProf.append((first_prof[0], second_prof[0], min_value * 2))

    if is_odd_matching:
        index = max(
            {
                k: v
                for k, v in difference_occurences.items()
                if nb_of_it_occurences_list[k]
            }.keys()
        )
        it_prof = nb_of_it_occurences_list[index].pop(0)
        id_ninfo_prof = nonInfoProf[0][0]
        it_prof[1] -= 1
        coupleDeProf.append((it_prof[0], id_ninfo_prof, 1))

    # coupler les profs avec le même nombre d'heure
    indexNonInfoProf = 0
    indexInfoProf = 0
    while indexInfoProf < len(infoProf) and indexNonInfoProf < len(nonInfoProf):
        if infoProf[indexInfoProf][1] == nonInfoProf[indexNonInfoProf][1]:
            coupleDeProf.append(
                (
                    infoProf[indexInfoProf][0],
                    nonInfoProf[indexNonInfoProf][0],
                    infoProf[indexInfoProf][1] * 2,
                )
            )
            del infoProf[indexInfoProf]
            del nonInfoProf[indexNonInfoProf]

        elif infoProf[indexInfoProf][1] > nonInfoProf[indexNonInfoProf][1]:
            indexInfoProf += 1

        else:
            indexNonInfoProf += 1

    # coupler les prof restant
    while len(nonInfoProf) > 0:
        if infoProf[0][1] > nonInfoProf[0][1]:
            coupleDeProf.append(
                (infoProf[0][0], nonInfoProf[0][0], nonInfoProf[0][1] * 2)
            )
            infoProf[0][1] -= nonInfoProf[0][1]
            del nonInfoProf[0]

        elif infoProf[0][1] == nonInfoProf[0][1]:
            coupleDeProf.append(
                (infoProf[0][0], nonInfoProf[0][0], nonInfoProf[0][1] * 2)
            )
            del infoProf[0]
            del nonInfoProf[0]

        else:
            coupleDeProf.append((infoProf[0][0], nonInfoProf[0][0], infoProf[0][1] * 2))
            nonInfoProf[0][1] -= infoProf[0][1]
            del infoProf[0]

    # coupler les profs info qui ont le même nombre d'heure
    indexInfoProf = 0
    while indexInfoProf < len(infoProf) - 1:
        if infoProf[indexInfoProf][1] == infoProf[indexInfoProf + 1][1]:
            coupleDeProf.append(
                (
                    infoProf[indexInfoProf][0],
                    infoProf[indexInfoProf + 1][0],
                    infoProf[indexInfoProf][1] * 2,
                )
            )
            del infoProf[indexInfoProf + 1]
            del infoProf[indexInfoProf]
        else:
            indexInfoProf += 1

    # coupler le reste des profs d'info
    while len(infoProf) > 0:
        if infoProf[0][1] > infoProf[1][1]:
            coupleDeProf.append((infoProf[0][0], infoProf[1][0], infoProf[1][1] * 2))
            infoProf[0][1] -= infoProf[1][1]
            del infoProf[1]

        elif infoProf[0][1] == infoProf[1][1]:
            coupleDeProf.append((infoProf[0][0], infoProf[1][0], infoProf[1][1] * 2))
            del infoProf[1]
            del infoProf[0]

        else:
            coupleDeProf.append((infoProf[0][0], infoProf[1][0], infoProf[0][1] * 2))
            infoProf[1][1] -= infoProf[0][1]
            del infoProf[1]

    return coupleDeProf


def match_vivas(graph: Graph, number_of_vivas_left: dict, k: int):
    """
    Calculate bipartite matching using the Hopcroft-Karp algorithm
    https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.bipartite.matching.hopcroft_karp_matching.html#networkx.algorithms.bipartite.matching.hopcroft_karp_matching
    """

    ordre_passage: list[list[str]] = []
    # bottom_nodes, top_nodes = bipartite.sets(graph)
    while graph.number_of_nodes() > 0:
        try:
            max_degree = max((d for n, d in graph.degree())) + 1
            max_attempts = max_degree * max_degree
            coloring = None
            while not coloring:
                try:
                    coloring = equitable_color(graph, max_degree)
                except Exception as e:
                    print(e)
                    max_degree += 1
                    if max_degree >= max_attempts:
                        raise ValueError("Vivas couldn't be matched")

            colors = set(coloring.values())
            occurences = dict()
            for color in colors:
                occurences[color] = 0
            for node in coloring:
                occurences[coloring[node]] += 1

            max_color = None
            max_occurence = 0
            for color, occurence in occurences.items():
                if occurence > max_occurence:
                    max_color = color

            vivas = list()
            counter = 0
            for viva, color in coloring.items():
                if color == max_color:
                    vivas.append(viva)
                    counter += 1
                    if counter >= k:
                        break

            ordre_passage.append(vivas.copy())

            for viva in vivas:
                number_of_vivas_left[viva] -= 1

            duos_to_remove = list()
            for duo, left in number_of_vivas_left.items():
                if left <= 0:
                    duos_to_remove.append(duo)

            for duo_to_remove in duos_to_remove:
                del number_of_vivas_left[duo_to_remove]
                graph.remove_node(duo_to_remove)

        except nx.AmbiguousSolution:
            break

    return ordre_passage


def get_time_slots(info_profs, non_info_profs, room_limit: int):
    choice_1_vivas = join_prof_1(info_profs.copy(), non_info_profs.copy())
    choice_2_vivas = join_prof_2(info_profs.copy(), non_info_profs.copy())

    choice_1_sum_vivas = sum([viva[2] for viva in choice_1_vivas])
    choice_2_sum_vivas = sum([viva[2] for viva in choice_2_vivas])

    vivas = []

    if choice_1_sum_vivas == choice_2_sum_vivas:
        choice_1_number_duos = len(choice_1_vivas)
        choice_2_number_duos = len(choice_2_vivas)
        if choice_1_number_duos < choice_2_number_duos:
            vivas = choice_1_vivas
        else:
            vivas = choice_2_vivas
    elif choice_1_sum_vivas < choice_2_sum_vivas:
        vivas = choice_1_vivas
    else:
        vivas = choice_2_vivas
    # vivas = [
    #     (1, 2, 4),
    #     (3, 8, 1),
    #     (1, 9, 6),
    #     (2, 11, 4),
    #     (6, 12, 2),
    #     (3, 8, 4),
    #     (4, 8, 4),
    #     (5, 10, 4),
    #     (7, 10, 2),
    # ]

    g = Graph()
    number_of_vivas_left = dict()
    profs = set()

    for viva in vivas:
        name = f"{viva[0]}&{viva[1]}"
        profs.add(viva[0])
        profs.add(viva[1])
        g.add_node(name)
        if name in number_of_vivas_left:
            number_of_vivas_left[name] += viva[2]
        else:
            number_of_vivas_left[name] = viva[2]

    conflict_list = list()
    for prof in profs:
        conflicts = list()
        for node in g.nodes:
            if node.endswith(f"&{prof}") or node.startswith(f"{prof}&"):
                conflicts.append(node)
        conflict_list.append(conflicts.copy())

    def add_edges_for_conflicts(graph: Graph, conflicts: list) -> Graph:
        if len(conflicts) > 1:
            for conflict in conflicts[1:]:
                graph.add_edge(conflicts[0], conflict)
            return add_edges_for_conflicts(graph, conflicts[1:])
        return graph

    for conflicts in conflict_list:
        g = add_edges_for_conflicts(g, conflicts)

    return match_vivas(g, number_of_vivas_left, room_limit)


infoProf = [[1, 5], [2, 4], [3, 3], [4, 2], [5, 2], [6, 1], [7, 1]]
nonInfoProf = [[8, 4], [9, 3], [10, 3], [11, 2], [12, 1]]
res = get_time_slots(infoProf, nonInfoProf, 10)
