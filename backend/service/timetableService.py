from model.company import Company
from service import excelConverter
from service.graphConverter import wishes_to_graph
from service.graphService import calculHopcroft
from service.graphConverter import graph_to_front

# from service.graphVizualization import display_graph


def calculerEmploiDuTemps(excel_bytes: bytes):
    """
    Convert the uploaded Excel file into a Wish object.
    """

    wish_list, slots = excelConverter.convert_to_wish(excel_bytes)
    graph = wishes_to_graph(wish_list)
    timetable = calculHopcroft(graph)

    return graph_to_front(timetable, slots)
