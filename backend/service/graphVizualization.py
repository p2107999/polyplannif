import matplotlib.pyplot as plt
import networkx as nx
from networkx import Graph
from networkx.algorithms import bipartite


def display_graph(graph: Graph):
    # plt.figure(figsize=(10, 10))

    # # positions for all nodes
    # bottom_nodes, top_nodes = bipartite.sets(graph)
    # pos = nx.bipartite_layout(graph, top_nodes)

    # # nodes
    # nx.draw_networkx_nodes(graph, pos, node_size=400, alpha=0.9)

    # # labels
    # nx.draw_networkx_labels(graph, pos, font_size=14, font_color='black', font_family='sans-serif')

    # # edges
    # nx.draw_networkx_edges(graph, pos, width=1)
    # nx.draw_networkx_edge_labels(graph, pos, font_color='red')

    # plt.axis('off')
    # plt.show()

    # plt.figure(figsize=(10, 10))

    nx.draw(graph, pos=nx.spring_layout(graph))
    plt.draw()

    # plt.axis("off")
    # plt.show()
