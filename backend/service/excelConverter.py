from typing import Tuple, List

import pandas as pd
from fastapi import UploadFile
from io import BytesIO

from model.company import Company
from model.student import Student
from model.voeux import Voeux
from model.teacher import Teacher
from model.room import Room


def read_excel_to_dataframe(excelbytes: bytes):
    """
    Read the uploaded Excel file and convert it into a Pandas DataFrame.
    """
    excel_data = BytesIO(excelbytes)  # Creating a BytesIO object from the file contents
    df = pd.read_excel(excel_data)  # Reading the Excel file into a DataFrame
    return df


def dataframe_to_wish(df: pd.DataFrame) -> list[Voeux]:
    student_list = []
    company_list = []
    voeux_list = []

    # Create Student instances
    for student_name in df.iloc[0, 2:].values:
        student_list.append(Student(student_name))

    for company_name in df.iloc[1:, 1].values:
        company_list.append(Company(company_name))

    for row_index, row in enumerate(df.iloc[1:, 2:].values):
        for col_index, value in enumerate(row):
            if pd.isna(value):
                continue
            if str(value).strip():
                voeux_list.append(
                    Voeux(company_list[row_index], student_list[col_index])
                )
    return voeux_list


def convert_to_wish(excelbytes: bytes) -> Tuple[List[Voeux], List[dict]]:
    """
    Convert the uploaded Excel file into a Wish object.
    """
    df = read_excel_to_dataframe(excelbytes)
    wishList = dataframe_to_wish(df)

    slots = list()
    df_slots = pd.read_excel(BytesIO(excelbytes), sheet_name=1)
    slot_header_beginning = df_slots.iloc[0, 1]
    slot_header_end = df_slots.iloc[0, 2]
    for _, row in df_slots.iterrows():
        beginning = row.iloc[1]
        end = row.iloc[2]
        if (
            pd.isna(beginning)
            or beginning == slot_header_beginning
            or pd.isna(end)
            or end == slot_header_end
        ):
            continue

        slots.append({"beginning": beginning, "end": end})
    return wishList, slots


def convert_to_teachers_rooms(excel_bytes: bytes):
    """
    Convert the uploaded Excel file into a list of computer science teachers, non-computer science teachers and a list
    of rooms.

    :param bytes excel_bytes: The Excel file to convert. The first sheet of the Excel file must contain the teachers and students data. The second sheet of the Excel file must contain the rooms' data.
    :return: tuple[list[Teacher], list[Teacher], list[Room]
    """

    computers_teachers = []
    non_computers_teachers = []
    students = []

    df_teachers_students = pd.read_excel(BytesIO(excel_bytes), sheet_name=0)

    student_names = df_teachers_students.iloc[0, 3:]
    for student_name in student_names:
        students.append(Student(student_name))

    for _, row in df_teachers_students.iterrows():
        teacher_name = row.iloc[1]
        if pd.isna(teacher_name) or teacher_name == "Prof/élève":
            continue

        teacher = Teacher(teacher_name)
        if str(row.iloc[2]).casefold() == "X".casefold():
            computers_teachers.append(teacher)
        else:
            non_computers_teachers.append(teacher)

        for student_name, has_student in zip(student_names, row[3:]):
            if str(has_student).casefold() == "X".casefold():
                student = next(s for s in students if s.name == student_name)
                teacher.add_student(student)

    rooms = []

    df_rooms = pd.read_excel(BytesIO(excel_bytes), sheet_name=1)
    room_header = df_rooms.iloc[0, 1]
    for _, row in df_rooms.iterrows():
        room_name = row.iloc[1]
        if pd.isna(room_name) or room_name == room_header:
            continue
        room = Room(room_name)
        rooms.append(room)

    return computers_teachers, non_computers_teachers, rooms
