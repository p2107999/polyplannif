from pydantic import Field, BaseModel
from typing import List
from model.pydantic.jobdating.TimeSlotSchema import TimeSlotSchema, sample

from model.company import Company

class CompanySchema(BaseModel):
    name: str = Field(description="Name of the company", example="Entreprise1")
    time_slots: List[TimeSlotSchema] = Field(
        description="Time slots of the company", example=[sample], alias="timeSlots"
    )

    def __eq__(self, obj):
        return (
            (isinstance(obj, Company) or isinstance(obj, CompanySchema))
            and obj.name == self.name
        ) or (isinstance(obj, str) and obj == self.name)

    class Config:
        populate_by_name = True
