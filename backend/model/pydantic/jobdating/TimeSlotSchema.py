from pydantic import Field, BaseModel
from typing import List
from model.pydantic.jobdating.AppointmentSchema import (
    AppointmentSchema,
    sample as sample_appointment,
)

# from model.pydantic._base import BaseAllowNameModel


class TimeSlotSchema(BaseModel):
    date: str = Field(description="Date of appointments", example="2000-01-01")
    appointments: List[AppointmentSchema] = Field(
        description="Appointments of the company", example=[sample_appointment]
    )

    class Config:
        populate_by_name = True


sample = TimeSlotSchema(date="2000-01-01", appointments=[sample_appointment])
