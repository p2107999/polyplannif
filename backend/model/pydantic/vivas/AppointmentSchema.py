from pydantic import Field, BaseModel

# from model.pydantic._base import BaseAllowNameModel


class AppointmentSchema(BaseModel):
    first_teacher: str = Field(
        "Name of the main prof", alias="firstTeacher", example="Prof1"
    )
    second_teacher: str = Field(
        "Name of the prof present as jury", alias="secondTeacher", example="Prof2"
    )
    student: str = Field(description="Name of the stuend", example="Eleve1")
    start_time: str = Field(
        description="Appointment beggining time", alias="startTime", example="10:15"
    )
    end_time: str = Field(
        description="Appointment beggining time", alias="endTime", example="10:30"
    )

    class Config:
        populate_by_name = True


sample = AppointmentSchema(
    **{
        "firstTeacher": "Prof1",
        "secondTeacher": "Prof2",
        "student": "Eleve1",
        "startTime": "10:15",
        "endTime": "10:30",
    }
)
