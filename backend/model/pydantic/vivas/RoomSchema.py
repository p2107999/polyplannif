from pydantic import Field, BaseModel
from typing import List
from model.pydantic.vivas.DaySchema import DaySchema, sample


class RoomSchema(BaseModel):
    room: str = Field(description="Name of the room", example="Salle1")
    time_slots: List[DaySchema] = Field(
        description="Time slots of vivas in this room",
        alias="timeSlots",
        example=[sample],
    )

    class Config:
        populate_by_name = True
