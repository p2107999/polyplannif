from pydantic import BaseModel


def BaseAllowNameModel(BaseModel):
    class Config:
        allow_population_by_field_name = True
