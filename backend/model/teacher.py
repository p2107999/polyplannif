class Teacher:
    def __init__(self, name):
        self.name = name
        self.supervised_students = []

    def add_student(self, student):
        self.supervised_students.append(student)
