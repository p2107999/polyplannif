from model.company import Company
from model.student import Student

class Voeux:
    """
    Reprensation of a "Wish"

    """
    def __init__(self, company: Company, student: Student) -> None:
        self.company = company
        self.student = student
    