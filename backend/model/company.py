# import model.pydantic.jobdating.CompanySchema


class Company:
    def __init__(self, name: str) -> None:
        """
        Name of the company

        Args:
            name (str): to give to
        """
        self.name = name

    def __str__(self) -> str:
        return str(self.name)
