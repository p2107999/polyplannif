class Student:
    def __init__(self, name: str) -> None:
        """
        Name of the student

        Args:
            name (str): to give name to the student
        """
        self.name = name

    def __str__(self) -> str:
        return str(self.name)

    def __eq__(self, obj):
        return (isinstance(obj, Student) and obj.name == self.name) or (isinstance(obj, str) and obj == self.name)