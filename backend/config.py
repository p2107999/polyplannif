import os

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

MICROSERVICE_URL = os.environ.get("MICROSERVICE_URL")
