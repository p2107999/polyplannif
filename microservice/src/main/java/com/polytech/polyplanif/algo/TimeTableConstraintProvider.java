package com.polytech.polyplanif.algo;

import com.polytech.polyplanif.domain.Meeting;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.api.score.stream.Constraint;
import org.optaplanner.core.api.score.stream.ConstraintFactory;
import org.optaplanner.core.api.score.stream.ConstraintProvider;
import org.optaplanner.core.api.score.stream.Joiners;

import java.time.Duration;

public class TimeTableConstraintProvider implements ConstraintProvider {
    @Override
    public Constraint[] defineConstraints(ConstraintFactory constraintFactory) {
        return new Constraint[]{
                // Hard
                roomConstraint(constraintFactory),
                tutorMustAssistToOneMeetingPerTimeSlot(constraintFactory),
                secondTeacherMustAssistToOneMeetingPerTimeSlot(constraintFactory),
                teachersMustAssistToOneMeetingPerTimeSlot(constraintFactory),
                // Soft
                tutorRoomStability(constraintFactory),
                secondTeacherRoomStability(constraintFactory),

                tutorTimeEfficiency(constraintFactory),
                secondTeacherTimeEfficiency(constraintFactory),

                makeMeetingAsSoonAsPossible(constraintFactory),
                minimizeTotalTimeSlot(constraintFactory),
        };
    }

    private Constraint roomConstraint(ConstraintFactory constraintFactory) {
        // A room can accommodate at most one lesson at the same time
        return constraintFactory
                // Select each pair of two different Meeting
                .forEachUniquePair(
                    Meeting.class,
                    // in the same timeslot
                    Joiners.equal(Meeting::getTimeSlot),
                    // in the same room
                    Joiners.equal(Meeting::getRoom)
                )
                // Penalize each pair
                .penalize(HardSoftScore.ONE_HARD)
                .asConstraint("Room conflict");
    }

    private Constraint minimizeTotalTimeSlot(ConstraintFactory constraintFactory) {
        return constraintFactory.forEach(Meeting.class)
                .join(Meeting.class)
                .filter((meeting1, meeting2) -> meeting1.getTimeSlot().getDay().getDayOfYear() - meeting2.getTimeSlot().getDay().getDayOfYear() == 0)
                .reward(HardSoftScore.ofSoft(5))
                .asConstraint("Minimize total time slot");
    }

    private Constraint teachersMustAssistToOneMeetingPerTimeSlot(ConstraintFactory constraintFactory) {
        // Teachers must make ONE Meeting at the same time
        return constraintFactory
                // Select each pair of two different Meeting
                .forEach(Meeting.class)
                .join(Meeting.class)
                .filter((meeting1, meeting2) -> meeting1.getTimeSlot().equals(meeting2.getTimeSlot()) && meeting1.getFirstTeacher().equals(meeting2.getSecondTeacher()))
                // penalize each pair with a Hard weight
                .penalize(HardSoftScore.ONE_HARD)
                .asConstraint("Teachers conflict");
    }

    private Constraint tutorMustAssistToOneMeetingPerTimeSlot(ConstraintFactory constraintFactory) {
        // Tutor must make ONE Meeting at the same time
        return constraintFactory
                // Select each pair of two different Meeting
                .forEachUniquePair(Meeting.class,
                        // in the same timeslot
                        Joiners.equal(Meeting::getTimeSlot),
                        // with same tutor
                        Joiners.equal(Meeting::getFirstTeacher)
                )
                // penalize each pair with a Hard weight
                .penalize(HardSoftScore.ONE_HARD)
                .asConstraint("Tutor conflict");
    }

    private Constraint secondTeacherMustAssistToOneMeetingPerTimeSlot(ConstraintFactory constraintFactory) {
        // Second Teacher must make ONE Meeting at the same time
        return constraintFactory
                // Select each pair of two different Meeting
                .forEachUniquePair(Meeting.class,
                        // in the same timeslot
                        Joiners.equal(Meeting::getTimeSlot),
                        // with same second teacher
                        Joiners.equal(Meeting::getSecondTeacher)
                )
                // penalize each pair with a Hard weight
                .penalize(HardSoftScore.ONE_HARD)
                .asConstraint("Second teacher conflict");
    }

    /*
        La diminution du creux entre 2 soutenances d'un prof fonctionne bien si on a beaucoup de salles
     */
    Constraint tutorTimeEfficiency(ConstraintFactory constraintFactory) {
        // A Tutor prefers to teach sequential lessons and dislikes gaps between lessons.
        return constraintFactory
                .forEach(Meeting.class)
                .join(Meeting.class,
                        // Meeting with same tutor
                        Joiners.equal(Meeting::getFirstTeacher),
                        // Meeting of a specific tutor, the same day
                        Joiners.equal((meeting) -> meeting.getTimeSlot().getDay()))
                .filter((meeting1, meeting2) -> {
                    Duration between = Duration.between(meeting1.getTimeSlot().getEndTime(),
                            meeting2.getTimeSlot().getStartTime());
                    // two meetings are separated by less than 30 minutes
                    return !between.isNegative() && between.compareTo(Duration.ofMinutes(30)) <= 0;
                })
                .reward(HardSoftScore.ONE_SOFT)
                .asConstraint("Tutor time efficiency");
    }

    Constraint secondTeacherTimeEfficiency(ConstraintFactory constraintFactory) {
        // Second teacher prefers to teach sequential lessons and dislikes gaps between lessons.
        return constraintFactory
                .forEach(Meeting.class)
                .join(Meeting.class,
                        // Meeting with same tutor
                        Joiners.equal(Meeting::getSecondTeacher),
                        // Meeting of a specific tutor, the same day
                        Joiners.equal((meeting) -> meeting.getTimeSlot().getDay()))
                .filter((meeting1, meeting2) -> {
                    Duration between = Duration.between(meeting1.getTimeSlot().getEndTime(),
                            meeting2.getTimeSlot().getStartTime());
                    // two meetings are separated by less than 30 minutes
                    return !between.isNegative() && between.compareTo(Duration.ofMinutes(30)) <= 0;
                })
                .reward(HardSoftScore.ONE_SOFT)
                .asConstraint("Second teacher time efficiency");
    }

    Constraint tutorRoomStability(ConstraintFactory constraintFactory) {
        // Teachers prefer to be in the same Room
        return constraintFactory
                // Select each pair of two different Meeting
                .forEachUniquePair(
                        Meeting.class,
                        // The same day
                        Joiners.equal(Meeting::getDay),
                        // With same teachers
                        Joiners.equal(Meeting::getFirstTeacher))
                // Penalize each pair with soft weight if another meeting is not at the same Room
                .filter(((meeting1, meeting2) -> !meeting1.getRoom().equals(meeting2.getRoom())))
                .penalize(HardSoftScore.ONE_SOFT)
                .asConstraint("Tutor room stability");
    }

    private Constraint secondTeacherRoomStability(ConstraintFactory constraintFactory) {
        // Teachers prefer to be in the same Room
        return constraintFactory
                // Select each pair of two different Meeting
                .forEachUniquePair(
                        Meeting.class,
                        // The same day
                        Joiners.equal(Meeting::getDay),
                        // With same teachers
                        Joiners.equal(Meeting::getSecondTeacher))
                // Penalize each pair with soft weight if another meeting is not at the same Room
                .filter(((meeting1, meeting2) -> !meeting1.getRoom().equals(meeting2.getRoom())))
                .penalize(HardSoftScore.ONE_SOFT)
                .asConstraint("Second teacher room stability");
    }

    private Constraint makeMeetingAsSoonAsPossible(ConstraintFactory constraintFactory) {
        return constraintFactory
                .forEach(Meeting.class)
                .penalize(HardSoftScore.ONE_SOFT, Meeting::getTimeSlotWeight) // 1 * getTimeSlotWeight
                .asConstraint("Meeting As Soon As Possible");
    }
}
