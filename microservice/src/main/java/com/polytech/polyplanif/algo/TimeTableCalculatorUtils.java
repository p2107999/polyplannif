package com.polytech.polyplanif.algo;

import com.polytech.polyplanif.domain.TimeSlot;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class TimeTableCalculatorUtils {
    public static List<TimeSlot> generateDays(LocalDate begin, int totalDays) {
        List<TimeSlot> timeSlots = new ArrayList<>();

        List<LocalDate> days = new ArrayList<>();
        days.add(begin);

        LocalDate localDate = begin;

        for (int i = 2; i <= totalDays ; i++) {
            localDate = localDate.plusDays(1);
            days.add(localDate);
        }

        int totalSlotPerDay = 11;
        long durationMinutes = 50;
        for (LocalDate day : days) {
            LocalTime beginMorning = LocalTime.of(8, 0);
            for (int i = 1; i <= 6; i++) {
                TimeSlot timeSlot = new TimeSlot(day, beginMorning, beginMorning.plusMinutes(durationMinutes));
                timeSlots.add(timeSlot);
                beginMorning = beginMorning.plusMinutes(50);
            }

            LocalTime beginAfterNoon = LocalTime.of(14, 0);
            for (int i = 7; i <= totalSlotPerDay; i++) {
                TimeSlot timeSlot = new TimeSlot(day, beginAfterNoon, beginAfterNoon.plusMinutes(durationMinutes));
                timeSlots.add(timeSlot);
                beginAfterNoon = beginAfterNoon.plusMinutes(50);
            }
        }

        return timeSlots;
    }
}
