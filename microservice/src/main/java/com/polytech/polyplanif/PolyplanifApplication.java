package com.polytech.polyplanif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PolyplanifApplication {

    public static void main(String[] args) {
        SpringApplication.run(PolyplanifApplication.class, args);
    }

}
