package com.polytech.polyplanif.controller.input;

public class MatchingTeachersInput {
    private final String teacher1;
    private final int teacher1StudentCount;
    private final String teacher2;
    private final int teacher2StudentCount;
    private final int totalSlot;

    public MatchingTeachersInput(String teacher1, int teacher1StudentCount, int teacher2TotalStudents, String teacher2, int totalSlot) {
        this.teacher1 = teacher1;
        this.teacher1StudentCount = teacher1StudentCount;
        this.teacher2 = teacher2;
        this.teacher2StudentCount = teacher2TotalStudents;
        this.totalSlot = totalSlot;
    }

    public String getTeacher1() {
        return teacher1;
    }

    public int getTeacher1StudentCount() {
        return teacher1StudentCount;
    }

    public String getTeacher2() {
        return teacher2;
    }

    public int getTeacher2StudentCount() {
        return teacher2StudentCount;
    }

    public int getTotalSlot() {
        return totalSlot;
    }
}
