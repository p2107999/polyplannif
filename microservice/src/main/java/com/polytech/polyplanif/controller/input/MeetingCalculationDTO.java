package com.polytech.polyplanif.controller.input;

import com.polytech.polyplanif.domain.Room;
import com.polytech.polyplanif.domain.Tutor;

import java.util.List;

public class MeetingCalculationDTO {
    private final List<MatchingTeachersInput> matchingTeachers;
    private final List<Tutor> tutors;
    private List<Room> rooms;

    public MeetingCalculationDTO(List<MatchingTeachersInput> matchingTeachers, List<Tutor> tutors, List<Room> rooms) {
        this.matchingTeachers = matchingTeachers;
        this.tutors = tutors;
        this.rooms = rooms;
    }

    public List<MatchingTeachersInput> getMatchingTeachers() {
        return matchingTeachers;
    }

    public List<Tutor> getTutors() {
        return tutors;
    }

    public List<Room> getRooms() {
        return rooms;
    }
}
