package com.polytech.polyplanif.controller;

import com.polytech.polyplanif.algo.TimeTableCalculatorUtils;
import com.polytech.polyplanif.controller.input.MatchingTeachersInput;
import com.polytech.polyplanif.controller.input.MeetingCalculationDTO;
import com.polytech.polyplanif.controller.output.AppointmentsDateByRoom;
import com.polytech.polyplanif.domain.Meeting;
import com.polytech.polyplanif.domain.Room;
import com.polytech.polyplanif.domain.TimeSlot;
import com.polytech.polyplanif.domain.TimeTable;
import com.polytech.polyplanif.domain.Student;
import com.polytech.polyplanif.domain.Tutor;
import org.optaplanner.core.api.solver.SolverJob;
import org.optaplanner.core.api.solver.SolverManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;


@RestController
@RequestMapping("generate/best-timetable")
public class MeetingCalculationController {
    @Autowired
    private SolverManager<TimeTable, UUID> solverManager;

    @PostMapping
    public List<AppointmentsDateByRoom> calculateTimeTableFromTutorsMatching(@RequestBody MeetingCalculationDTO meetingCalculationDTO) {
        List<Room> availableRooms = meetingCalculationDTO.getRooms();
        List<Tutor> tutors = meetingCalculationDTO.getTutors();

        // Create outdated meeting
        List<Meeting> outdatedMeetings = new ArrayList<>();
        long id = 1L;
        for (MatchingTeachersInput matchingTeacher : meetingCalculationDTO.getMatchingTeachers()) {
            String teacher1 = matchingTeacher.getTeacher1();
            String teacher2 = matchingTeacher.getTeacher2();

            Tutor tutor1 = tutors.stream().filter(tutor -> tutor.getName().equals(teacher1)).toList().get(0);
            Tutor tutor2 = tutors.stream().filter(tutor -> tutor.getName().equals(teacher2)).toList().get(0);

            // Take n students which are not outdated
            List<Student> studentsNotYetPlacedFromTeacher1 = tutor1.getStudentsNotInMeeting(matchingTeacher.getTeacher1StudentCount());
            List<Student> studentsNotYetPlacedFromTeacher2 = tutor2.getStudentsNotInMeeting(matchingTeacher.getTeacher2StudentCount());

            for (Student studentFromTeacher1NotYetPlaced : studentsNotYetPlacedFromTeacher1) {
                Meeting meeting = new Meeting(id, tutor1, tutor2, studentFromTeacher1NotYetPlaced);
                outdatedMeetings.add(meeting);
                id++;
            }

            for (Student studentFromTeacher2NotYetPlaced : studentsNotYetPlacedFromTeacher2) {
                Meeting meeting = new Meeting(id, tutor1, tutor2, studentFromTeacher2NotYetPlaced);
                outdatedMeetings.add(meeting);
                id++;
            }
        }

        // TimeSlots
        /*
         Imagine we have only ONE Room. We can make all meetings sequentially in worst case
         */
        int worstDuration = (int)(Math.ceil((double) outdatedMeetings.size() / 11));
        List<TimeSlot> timeSlots = TimeTableCalculatorUtils.generateDays(LocalDate.now(), worstDuration);

        TimeTable problemTimeTable = new TimeTable(timeSlots, availableRooms, outdatedMeetings);
        UUID problemId = UUID.randomUUID();

        TimeTable solutionTimeTable;
        SolverJob<TimeTable, UUID> solverJob = this.solverManager.solve(problemId, problemTimeTable);

        try {
            solutionTimeTable = solverJob.getFinalBestSolution();
        } catch (ExecutionException | InterruptedException e) {
            throw new IllegalStateException("Solving failed.", e);
        }

        return AppointmentsDateByRoom.fromMeetingToMeetingsByRoom(solutionTimeTable.getMeetings());
    }
}
