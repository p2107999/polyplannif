package com.polytech.polyplanif.controller.output;

import com.polytech.polyplanif.domain.Meeting;
import com.polytech.polyplanif.domain.Room;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AppointmentsDateByRoom {
    private String room;
    private List<AppointmentsDate> timeSlots;

    public AppointmentsDateByRoom(String room, List<AppointmentsDate> timeSlots) {
        this.room = room;
        this.timeSlots = timeSlots;
    }

    public String getRoom() {
        return room;
    }

    public List<AppointmentsDate> getTimeSlots() {
        return timeSlots;
    }

    public static List<AppointmentsDateByRoom> fromMeetingToMeetingsByRoom(List<Meeting> meetings) {
        HashMap<Room, List<Meeting>> meetingsByRoom = new HashMap<>();
        for (Meeting meeting : meetings) {
            if (!meetingsByRoom.containsKey(meeting.getRoom())) {
                List<Meeting> initialMeetingsForARoom = new ArrayList<>();
                initialMeetingsForARoom.add(meeting);
                meetingsByRoom.put(meeting.getRoom(), initialMeetingsForARoom);
            } else {
                meetingsByRoom.get(meeting.getRoom()).add(meeting);
            }
        }

        List<AppointmentsDateByRoom> appointmentsDateByRooms = new ArrayList<>();

        for (Room room : meetingsByRoom.keySet()) {
            List<Meeting> meetingsInSpecificRoom = meetingsByRoom.get(room);
            List<AppointmentsDate> appointmentsDates = AppointmentsDate.fromMeetingsToAppointmentsDate(meetingsInSpecificRoom);
            AppointmentsDateByRoom appointmentsDateByRoom = new AppointmentsDateByRoom(room.getName(), appointmentsDates);
            appointmentsDateByRooms.add(appointmentsDateByRoom);
        }

        return appointmentsDateByRooms;
    }
}
