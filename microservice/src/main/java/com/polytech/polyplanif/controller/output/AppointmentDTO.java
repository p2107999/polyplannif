package com.polytech.polyplanif.controller.output;

import com.polytech.polyplanif.domain.Meeting;

import java.time.LocalTime;

public record AppointmentDTO(
        LocalTime startTime,
        LocalTime endTime,
        String firstTeacher,
        String secondTeacher,
        String student
) {
    public static AppointmentDTO fromMeeting(Meeting meeting) {
        return new AppointmentDTO(
                meeting.getTimeSlot().getStartTime(),
                meeting.getTimeSlot().getEndTime(),
                meeting.getFirstTeacher().getName(),
                meeting.getSecondTeacher().getName(),
                meeting.getStudent().getName()
        );
    }
}
