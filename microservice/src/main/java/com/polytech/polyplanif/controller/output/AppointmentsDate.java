package com.polytech.polyplanif.controller.output;

import com.polytech.polyplanif.domain.Meeting;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AppointmentsDate {
    private LocalDate date;
    private List<AppointmentDTO> appointments;

    public AppointmentsDate(LocalDate date, List<AppointmentDTO> appointments) {
        this.date = date;
        this.appointments = appointments;
    }

    public LocalDate getDate() {
        return date;
    }

    public List<AppointmentDTO> getAppointments() {
        return appointments;
    }

    public static List<AppointmentsDate> fromMeetingsToAppointmentsDate(List<Meeting> meetings) {
        HashMap<LocalDate, List<Meeting>> meetingsByDates = new HashMap<>();

        for (Meeting meeting : meetings) {
            LocalDate day = meeting.getTimeSlot().getDay();
            if (!meetingsByDates.containsKey(day)) {
                ArrayList<Meeting> initialMeetingsForDay = new ArrayList<>();
                initialMeetingsForDay.add(meeting);
                meetingsByDates.put(day, initialMeetingsForDay);
            } else {
                meetingsByDates.get(day).add(meeting);
            }
        }

        List<AppointmentsDate> appointmentsDates = new ArrayList<>();

        for (LocalDate day : meetingsByDates.keySet().stream().sorted().toList()) {
            List<Meeting> meetingsByDay = meetingsByDates.get(day);

            List<AppointmentDTO> appointmentDTOS = new ArrayList<>();
            for (Meeting meetingByDay : meetingsByDay) {
                appointmentDTOS.add(AppointmentDTO.fromMeeting(meetingByDay));
            }

            appointmentsDates.add(new AppointmentsDate(day, appointmentDTOS));
        }

        return appointmentsDates;
    }
}
