package com.polytech.polyplanif.domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.lookup.PlanningId;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import java.time.LocalDate;

@PlanningEntity
public class Meeting {
    @PlanningId
    private Long id;
    @PlanningVariable
    private TimeSlot timeSlot;
    @PlanningVariable
    private Room room;
    /**
     * Le tuteur de l'élève
     */
    private Tutor firstTeacher;
    /**
     * Le prof candide
     */
    private Tutor secondTeacher;
    private Student student;

    protected Meeting() {

    }

    public Meeting(Long id, Tutor firstTeacher, Tutor secondTeacher, Student student) {
        this.id = id;
        this.firstTeacher = firstTeacher;
        this.secondTeacher = secondTeacher;
        this.student = student;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setTimeSlot(TimeSlot timeSlot) {
        this.timeSlot = timeSlot;
    }

    public TimeSlot getTimeSlot() {
        return timeSlot;
    }

    public Room getRoom() {
        return room;
    }

    public Tutor getFirstTeacher() {
        return firstTeacher;
    }

    public Tutor getSecondTeacher() {
        return secondTeacher;
    }

    public Student getStudent() {
        return student;
    }

    public Long getId() {
        return id;
    }

    public LocalDate getDay() {
        return this.timeSlot.getDay();
    }

//    @Override
//    public int compareTo(Meeting meeting) {
//        // -1 arg meeting is less than this
//        // +1 arg meeting is much than this
//        if(this.timeSlot.getDay().isAfter(meeting.timeSlot.getDay())) {
//            return -1;
//        } else {
//            if(meeting.timeSlot.getDay().isAfter(timeSlot.getDay()) || meeting.timeSlot.getStartTime().isAfter(timeSlot.getStartTime())) {
//                return 1;
//            } else if(timeSlot.getStartTime().isAfter(meeting.timeSlot.getStartTime())) {
//                return -1;
//            } else {
//                return 0;
//            }
//        }
//    }

    @Override
    public String toString() {
        return "Meeting{" +
                "timeSlot=" + timeSlot +
                ", room=" + room +
                ", tutorTeacher='" + firstTeacher + '\'' +
                ", secondTeacher='" + secondTeacher + '\'' +
                ", student='" + student + '\'' +
                '}';
    }

    public Integer getTimeSlotWeight() {
        if(timeSlot != null) {
            return this.timeSlot.getStartTime().getHour() - 8;
        }
        return null;
    }
}
