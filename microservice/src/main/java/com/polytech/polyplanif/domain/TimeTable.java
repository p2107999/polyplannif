package com.polytech.polyplanif.domain;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

import java.util.List;

@PlanningSolution
public class TimeTable {
    @ValueRangeProvider
    @ProblemFactCollectionProperty
    private List<TimeSlot> timeSlots;

    @ValueRangeProvider
    @ProblemFactCollectionProperty
    private List<Room> rooms;

    @PlanningEntityCollectionProperty
    private List<Meeting> meetings;

    @PlanningScore
    private HardSoftScore score;

    public TimeTable() { }

    public TimeTable(List<TimeSlot> timeSlots, List<Room> rooms, List<Meeting> meetings) {
        this.timeSlots = timeSlots;
        this.rooms = rooms;
        this.meetings = meetings;
    }

    public List<TimeSlot> getTimeSlots() {
        return timeSlots;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public List<Meeting> getMeetings() {
        return meetings;
    }

    public HardSoftScore getScore() {
        return score;
    }
}
