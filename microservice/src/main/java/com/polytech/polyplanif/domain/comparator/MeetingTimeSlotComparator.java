package com.polytech.polyplanif.domain.comparator;

import com.polytech.polyplanif.domain.Meeting;

import java.util.Comparator;

public class MeetingTimeSlotComparator implements Comparator<Meeting> {
    @Override
    public int compare(Meeting m1, Meeting m2) {
        // -1 arg meeting is less than this
        // +1 arg meeting is much than this
        if(m1.getTimeSlot().getDay().isAfter(m2.getTimeSlot().getDay())) {
            return -1;
        } else {
            if(m2.getTimeSlot().getDay().isAfter(m1.getTimeSlot().getDay()) || m2.getTimeSlot().getStartTime().isAfter(m1.getTimeSlot().getStartTime())) {
                return 1;
            } else if(m1.getTimeSlot().getStartTime().isAfter(m2.getTimeSlot().getStartTime())) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
