package com.polytech.polyplanif.domain;

public class Student {
    private String name;
    private boolean isAddedToOutdatedMeeting = false;

    public Student() {}

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isAddedToOutdatedMeeting() {
        return this.isAddedToOutdatedMeeting;
    }

    public void addToOutDatedMeeting() {
        this.isAddedToOutdatedMeeting = true;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }
}
