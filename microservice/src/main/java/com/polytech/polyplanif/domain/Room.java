package com.polytech.polyplanif.domain;

import org.optaplanner.core.api.domain.lookup.PlanningId;

import java.util.Objects;

public class Room {
    @PlanningId
    private String name;

    protected Room() {
    }

    public Room(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "Room{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return Objects.equals(name, room.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
