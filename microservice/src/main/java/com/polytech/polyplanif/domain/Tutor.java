package com.polytech.polyplanif.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Tutor {
    private String name;
    private List<Student> students = new ArrayList<>();

    public Tutor() {

    }

    public Tutor(String name, boolean info) {
        this.name = name;
    }

    public Tutor(String name, boolean info, List<Student> students) {
        this(name, info);
        this.students = students;
    }

    public List<Student> getStudents() {
        return students;
    }

    public List<Student> getStudentsNotInMeeting(int total) {
        List<Student> studentsNotInMeeting = new ArrayList<>();
        int i = 0;
        boolean finish = false;
        if(total == 0) {
            return new ArrayList<>();
        }

        while (!finish) {

            Student student = this.students.get(i);
            if (!student.isAddedToOutdatedMeeting()) {
                studentsNotInMeeting.add(student);
                student.addToOutDatedMeeting();
            }

            i++;
            finish = studentsNotInMeeting.size() == total || i == this.students.size();
        }

        return studentsNotInMeeting;
    }

    public String getName() {
        return name;
    }

    public int totalStudents() {
        return this.students.size();
    }

    @Override
    public String toString() {
        return "Tutor{" +
                "name=" + this.name +
                "students=" + students +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tutor tutor = (Tutor) o;
        return Objects.equals(name, tutor.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
