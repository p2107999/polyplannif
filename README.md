# PolyPlannif

Bienvenue sur la documentation de **PolyPlannif**, une application moderne de planification, lancée via Docker Compose.

## 🚀 Lancement rapide

Pour démarrer l'application **PolyPlannif**, assurez-vous d'avoir Docker et Docker Compose installés sur votre machine. Ensuite, suivez ces étapes :

1. Clonez le dépôt GitLab : 
```bash
git clone https://forge.univ-lyon1.fr/p2107999/polyplannif.git
```
2. Naviguez vers le répertoire du projet : 
```bash
cd polyplannif
```
3. Lancez l'application avec Docker Compose : 
```
docker-compose up
```
4. Ouvrez l'application dans votre navigateur : 
[http://localhost:80](http://localhost:80)

## 📂 Structure du projet

Le projet est structuré en plusieurs sous-dossiers principaux :

- `frontend/` : Contient le code source de l'interface utilisateur.
- `backend/` : Contient le code source du serveur et la logique métier.
- `microservice/` : Contient le code des microservices pour les fonctionnalités spécifiques.

## 🆘 Support

Pour toute question ou problème, veuillez ouvrir un ticket sur GitLab.

## 🌟 Contribuer

Nous encourageons les contributions ! Si vous souhaitez contribuer, veuillez lire notre `CONTRIBUTING.md` pour plus d'informations sur le processus de soumission de contributions. Pour garantir une expérience de contribution fluide et productive, veuillez suivre les directives ci-dessous.

### 🌐 Frontend

Pour contribuer au frontend de **PolyPlannif** :

1. **Installation des dépendances** :
   Ouvrez un terminal et exécutez :
   ```bash
   npm install
   ```
   Cela installera toutes les dépendances nécessaires pour le projet frontend.

2. **Lancement en mode développement** :
   Pour démarrer le serveur de développement :
   ```bash
   npm start
   ```
   Cela lancera l'application frontend sur `http://localhost:3000` avec un rechargement automatique lors des modifications du code.

### 🔙 Backend

Pour contribuer au backend de **PolyPlannif** :

1. **Installation des dépendances avec Pipenv** :
   ```bash
   pip install pipenv
   pipenv install
   ```
   Cela crée un environnement virtuel et installe les dépendances définies dans `Pipfile`.

2. **Lancement du backend en mode développement** :
   ```bash
   pipenv run start
   ```
   Cela démarre le serveur backend sur `http://localhost:8000` avec rechargement automatique.
   
   Documentation de l'API Swagger : `http://localhost:8000/docs`

### 🔄 Microservices

Pour travailler avec les microservices de **PolyPlannif** :

1. **Avec IntelliJ IDEA** *(recommandé)* :
   Ouvrez le projet dans IntelliJ IDEA et lancez les microservices en utilisant Maven intégré.

2. **Sans IntelliJ IDEA** :
   - **Installation des dépendances** :
     ```bash
     mvn install
     ```
     Cela construira les microservices et installera les dépendances nécessaires.

   - **Lancement en mode développement** :
     ```bash
     mvn spring-boot:run
     ```
     Cela démarre le microservice sélectionné, par défaut sur `http://localhost:8001`.

---

**🔍 Testez votre travail** : Avant de soumettre vos contributions, veuillez tester votre code localement pour vous assurer qu'il fonctionne comme prévu.

**📝 Documentation** : Ajoutez des commentaires et mettez à jour la documentation si nécessaire pour refléter vos changements ou ajouts.

**🔄 Pull Request** : Soumettez une Pull Request avec une description claire de vos modifications. N'oubliez pas d'indiquer les issues que votre contribution résout.

Nous apprécions votre implication dans l'amélioration de **PolyPlannif** ! 🎉
```